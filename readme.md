# Recent changes
* version 0.0.3: third release - added a method that looks for a folder regex, that then returns the root folder. Fx, if a folder contains ".git" then the method will return the parent folder of .git.
* version 0.0.2: second release - now with a general function to find root folders
* version 0.0.1: first working release

# Known issues
* toolbox--execute will return `nil` if subfolder is parent to current file represented by buffer. This issue is reported [here](https://github.com/mslot/toolbox/issues/1)

# Missing features
* not yet possible to send in arguments to the script to be executed

# Why
This has been implemented by me to strengthen the tooling for a project I am developing. Right now the project resides in private repos, until it is stable enough to release, but this bit of tooling, is stable enough to be made open for the public.

Enjoy!

## Overview
This small collection of functions that can help with different things:

1. locate root folder of a project, and executing what ever script you have defined, in what ever folder you have defined. The root folder is defined as this:"The parent folder of the src folder". If you have no src folder, well then you have no root folder. The collection of functions defined in `toolbox` will then find the sibling `subfolder` to `src`, if it exist, and execute what ever script you have defined, if it exists. You could now be standing anywhere in the project, and execute a command that locates the `subfolder` folder (a sibling to the `src` folder) and execute the script you have configured.

Currently only python3 is supported, but feel free to chip in with a PR, if you like this, and want it to support more.

More features to come in the coming time, when i need more in my emacs setup.

## Good to know
The root locate looks for the _first_ parent folder to a src folder. So if you are on a path with multiple src folders, this will return the path minus the first src. Example: /src/path/to/src/project/folder. This will return /src/path/to/.

## How to use

* Clone this repo to a place you seem fit 

after cloning add this to your dot file:

```lisp
(load "path/to/cloned/dir/src/toolbox.el) 
```

If you reload, you will now get the following method to your disposal: `toolbox--execute`. It takes 3 arguments:

1. `mode`: only implemented mode right now is `'python3`
2. `subfolder`: name of the sibling of `src` folder
3. `script-name`: name of the script that resides in `subfolder` to execute

## Use the method
Currently I have a seperate file named `toolbox-extensions.el` that i load in my dot file:

```lisp
(load "path/to/toolbox-extension.el")
```

where I have methods to invoke scripts. Fx I have a `pack.py` that I need to invoke:

```lisp
(defun toolbox-pack ()
  (interactive)
  (message "%s"
           (toolbox--execute 'python3 "tools" "pack.py")))
```

I can now invoke this script by calling `M-x toolbox-pack` and if I am in a subtree to to the `src` folder, the `toolbox--execute` will traverse up, find the parent folder to the `src` folder, go into the `tools`, if it exists, and execute `pack.py` if it exists.

## Why not use projectile?
I wanted to learn som elisp, and I didn't want to be dependant on a third party library for traversing to root of a project.

## Missing core features
Currently you can't pass arguments to the script, but this is something I am working on implementing, because, well, I need it :). Also it could be great to not have the src hardcoded into the root locate function.

