(defun toolbox--execute(mode subfolder script-name)
  "Executes the script named 'scipt-name' located in 'subfolder' if the subfolder and script exists. Subfolder needs to be a sibling to src, so the src folder needs to be in place in the project. It traverse the path from the current buffer-file-name looking for src. If it finds it, it tags the parent folder to src as root, and the looks for 'subfolder'. Right now only python3 mode is supported."
  (let ((root-folder-path))
    (if (not (eq buffer-file-name nil))
        (setq root-folder-path (toolbox--locate-parent-to-src-folder-path buffer-file-name)))
    (when (not (eq root-folder-path nil))
      (nconc root-folder-path
             (list subfolder))
      (toolbox--execute-if-exist mode root-folder-path
                        script-name))))

(defun toolbox--locate-folder-path (start-path lookup-folder-regex)
  "This function loops backwards from start-path, until it finds a folder which matches a folder regex named, lookup-folder-regex. It then returns an array of names folders that represent the path."
  (let* ((dir-array (split-string start-path "/"))
         (dir-array-count (length dir-array))
         (root-folder-path))
    (while (and (>= dir-array-count 0) (eq root-folder-path nil))
      (let ((dir-string (mapconcat 'identity
                                   (seq-take dir-array dir-array-count)
                                   "/")))
            (if
                (eq
                 (length
                  (directory-files dir-string lookup-folder-regex)) 3)
                (setq root-folder-path (seq-take dir-array (1- dir-array-count)))
                )
            (setq dir-array-count (1- dir-array-count))))
    root-folder-path)
  )

(defun toolbox--locate-parent-folder-path (start-path child)
  "This function returns the parent path to child folder specified, or nil. start-path is where the lookup starts from. This returns a sequence of folder names that has to be concat'ed (with fx /) to a path string."
  (let* ((dir-array (split-string start-path "/"))
            (dir-array-count (length dir-array))
            (root-folder-path))
       (while (>= dir-array-count 0)
         (let ((current-element (elt dir-array
                                     (1- dir-array-count))))
           (if (equal current-element child)
               (setq root-folder-path (seq-take dir-array
                                                (1- dir-array-count))))
           (setq dir-array-count (1- dir-array-count))))
       root-folder-path)
  )

(defun toolbox--locate-parent-to-src-folder-path (start-path)
  "Locates the root folder of the project. This function assumes that the src folder is located as a child to root, and therefore it walks the directory tree until it finds the src folder, then it return the parent path. If src folder isn't found, this returns nil. This returns a sequence of folder names that has to be concat'ed (with fx /) to a path string."
  (toolbox--locate-parent-folder-path start-path "src")
 )

(defun toolbox--execute-if-exist (mode path file)
  "Execute the 'file' at 'path' in 'mode' if 'path' exists and 'file' exists within 'path'. Currently only python3 is supported. Mode and python3 is compared using eq so make sure to use default obarray when passing the 'python3 syombol to mode."
  (let* ((complete-path (append path
                                (list file)))
         (command (concat (mapconcat 'identity complete-path "/"))))
    (when (equal mode 'python3)
      (when (file-directory-p (mapconcat 'identity path "/"))
        (when (file-exists-p command)
         (message "Executing command %s with python3" command) 
         (shell-command-to-string (concat "python3 " "\"" command "\"")))))))
